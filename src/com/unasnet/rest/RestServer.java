package com.unasnet.rest;

import javax.print.attribute.standard.Media;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.json.JSONObject;

import com.google.gson.Gson;
import com.unasnet.basecontrol.entity.Base;
import com.unasnet.basecontrol.entity.TerminalTractor;
import com.unasnet.basecontrol.entity.Zone;

@Path("/restserver")
public class RestServer extends DBImpl {

	@Path("/get/base/{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getBase(@PathParam("id") String id) {
		
		Base base = (Base)super.get(id, Base.class);
		
		Gson gson = new Gson();
		return gson.toJson(base).toString();
	}
	@Path("/get/terminaltractor/{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getTerminalTractor(@PathParam("id") String id) {
		
		TerminalTractor tractor = (TerminalTractor)super.get(id, TerminalTractor.class);
		
		Gson gson = new Gson();
		return gson.toJson(tractor).toString();
	}
	@Path("/get/zone/{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getZone(@PathParam("id") String id) {
		
		Zone zone = (Zone)super.get(id, Zone.class);
		
		Gson gson = new Gson();
		return gson.toJson(zone).toString();
	}
	
	@Path("/delete/base/{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String deleteBase(@PathParam("id") String id) {
		
		return super.delete(id, Base.class).toString();
	}
	@Path("/delete/terminaltractor/{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String deleteTerminalTractor(@PathParam("id") String id) {
		
		return super.delete(id, TerminalTractor.class).toString();
	}
	@Path("/delete/zone/{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String deleteZone(@PathParam("id") String id) {
		
		return super.delete(id, Zone.class).toString();
	}
	
	@POST
	@Path("/put/base/")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String putBase(String json) {
		
		Gson gson = new Gson();
		Base newBase = gson.fromJson(json, Base.class);
		
		return super.put(newBase, Base.class).toString();
	}
	@POST
	@Path("/put/terminaltractor/")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String putTerminalTractor(String json) {
		
		Gson gson = new Gson();
		TerminalTractor newTractor = gson.fromJson(json, TerminalTractor.class);
		
		return super.put(newTractor, TerminalTractor.class).toString();
	}
	@POST
	@Path("/put/zone/")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String putZone(String json) {
		
		Gson gson = new Gson();
		Zone newZone = gson.fromJson(json, Zone.class);
		
		return super.put(newZone, Zone.class).toString();
	}
	
	@POST
	@Path("/update/base/")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String updateBase(String json) {
		
		Gson gson = new Gson();
		Base updateBase = gson.fromJson(json, Base.class);
		
		return super.update(updateBase, Base.class).toString();
	}
	@POST
	@Path("/update/terminaltractor/")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String updateTerminalTractor(String json) {
		
		Gson gson = new Gson();
		TerminalTractor updateTerminalTractor = gson.fromJson(json, TerminalTractor.class);
		
		return super.update(updateTerminalTractor, TerminalTractor.class).toString();
	}
	@POST
	@Path("/update/zone/")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String updateZone(String json) {
		
		Gson gson = new Gson();
		Zone updateZone = gson.fromJson(json, Zone.class);
		
		return super.update(updateZone, Zone.class).toString();
	}
}

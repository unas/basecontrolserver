package com.unasnet.rest;

import java.io.Serializable;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.json.JSONObject;

import com.unasnet.rest.hibernate.util.HibernateUtil;

public class DBImpl {
	
	/**
	 * Gets the object from database
	 * @param id
	 * @param classType
	 * @return
	 */
	protected Object get(Serializable id, Class<?> classType) {
		
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction tx = session.beginTransaction();
		Object returnValue = session.get(classType, id);
		
		tx.commit();
		
		return returnValue;
	}
	
	/**
	 * Deletes the object from database
	 * @param id
	 * @param classType
	 * @return
	 */
	protected JSONObject delete(Serializable id, Class<?> classType) {
		
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction tx = session.beginTransaction();
		Object removeValue = session.get(classType, id);
		JSONObject returnValue = new JSONObject();
		StringBuilder errorText = new StringBuilder();
		boolean success = false;
		
		if (removeValue != null) {
			session.remove(removeValue);
			success = true;
			
			tx.commit();
		}
		else {
			success = false;
			errorText.append("Could not find row to delete with id \"" + id + "\"");
			
			tx.rollback();
		}
		
		returnValue.put("success", success);
		returnValue.put("id", id);
		returnValue.put("error", errorText.toString());
		
		return returnValue;
	}
	
	protected JSONObject put(Object value, Class<?> classType) {
		
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction tx = session.beginTransaction();
		JSONObject returnValue = new JSONObject();
		StringBuilder errorText = new StringBuilder();
		boolean success = false;
		
		if (value != null) {
			session.save(value);
			success = true;
			
			tx.commit();
		}
		else {
			success = false;
			
			errorText.append("Could not put new value");
			tx.rollback();
		}
		
		returnValue.put("success", success);
		returnValue.put("error", errorText.toString());
		
		return returnValue;
	}
	
	protected JSONObject update(Object value, Class<?> classType) {
		
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		Transaction tx = session.beginTransaction();
		JSONObject returnValue = new JSONObject();
		StringBuilder errorText = new StringBuilder();
		boolean success = false;
		
		if (value != null) {
			session.update(value);
			success = true;
			
			tx.commit();
		}
		else {
			success = false;
			
			errorText.append("Could not update value");
			tx.rollback();
		}
		
		returnValue.put("success", success);
		returnValue.put("error", errorText.toString());
		
		return returnValue;
	}
}

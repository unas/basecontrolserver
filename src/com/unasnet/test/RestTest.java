package com.unasnet.test;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.json.JSONObject;

import com.unasnet.basecontrol.entity.Base;
import com.unasnet.rest.hibernate.util.HibernateUtil;


@Path("/ctofservice")
public class RestTest {
	

	
	@Path("{c}")
	@GET
	@Produces("application/json")
	public String convertCtoFfromInput(@PathParam("c") Double celsius) {

		//SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		//Session session = sessionFactory.openSession();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		
		Transaction tx = session.beginTransaction();
		
		
		
		Base base = new Base();
		base.setID("deleteMe");
		base.setName("Totally new name to delete");
		//session.save(base);
		
		
		//Base retrievedBase = (Base) session.get(Base.class, "deleteMe");
		//session.remove(retrievedBase);
		
		JSONObject returnValue = new JSONObject();
		
		returnValue.put("success", true);
		
		tx.commit();
		
		return returnValue.toString();
		

	}
}